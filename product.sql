
drop table if exists Auction;
drop table if exists Auction_log;
drop table if exists Product;

CREATE TABLE Product
(
   PROD_ID    varchar(10) NOT NULL,
   PROD_Name  VARCHAR(50) NOT NULL,
   Bid   float(10,2),
   PRIMARY KEY(PROD_ID)
)Engine = InnoDB;

CREATE TABLE Auction_log
(
   Action    varchar(20) NOT NULL,
   AUC_ID  VARCHAR(10) NOT NULL,
   TimeStamp datetime NOT NULL,
   PROD_ID varchar(10) NOT NULL,
   Bid    float(10,2),
   PRIMARY KEY(TimeStamp),
   foreign key(PROD_ID) references Product(PROD_ID)
)Engine = InnoDB;

CREATE TABLE Auction
(
   AUC_ID    int not null AUTO_INCREMENT,
   TimeStamp datetime not null,
   PROD_ID  VARCHAR(10) NOT NULL,
   Bid   float(10,2),
   PRIMARY KEY(AUC_ID),
   foreign key(TimeStamp) references Auction_log(TimeStamp)
)Engine = InnoDB;

INSERT INTO Product VALUES('3','Computer',1000.00);
INSERT INTO Product VALUES('4','CPU',1500.00);
INSERT INTO Product VALUES('5','Monitor',2500.00);
