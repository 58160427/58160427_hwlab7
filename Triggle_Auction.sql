
drop trigger if exists auction_operation;

DELIMITER $$
CREATE TRIGGER auction_operation
        AFTER INSERT ON Auction_log
        FOR EACH ROW
BEGIN
        IF (NEW.Action='create') THEN
                 IF(NEW.Bid>0) THEN
                        insert into Auction(TimeStamp,PROD_ID,Bid) values (NEW.TimeStamp,NEW.PROD_ID,NEW.Bid);

                 END IF;

        elseif (NEW.Action='update') then
                select Bid into @OLD_Bid
                from Auction
                where PROD_ID = NEW.PROD_ID;

                        if (NEW.Bid > @OLD_Bid) then
                        update Auction
                        set Bid = NEW.Bid
                        ,TimeStamp = NEW.TimeStamp
                        where PROD_ID = NEW.PROD_ID;
                        end if;

        elseif (NEW.Action='delete') then
                select Bid into @OLD_Bid
                from Auction
                where PROD_ID = NEW.PROD_ID;

                        IF (NEW.Bid = OLD_Bid) THEN
                        delete from Auction
                        WHERE PROD_ID = NEW.PROD_ID;
                        END IF;
        end if;

END $$
